import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { ProveedorInterface } from '../interfaces/proveedor.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

import 'rxjs/add/operator/map';


@Injectable()
export class ProveedorService {
  constructor(
      private sett:Settings,
      private _ls:LoginService,
      private _es:ErrorService,
			private http:Http) { }

  
  get(){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/proveedor?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  getProveedoresUnicos(){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/proveedor/unicos?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  // getProveedoresxNegocio(){
  //  let token = this._ls.getToken();
  //  return this.http.get(`${this.sett.url}/proveedor/por-negocio?token=${token}`)
  //             .map(data => {
  //              return data.json();
  //             })
  // }

  getCarterasxProveedor(id_proveedor){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/proveedor/carteras/${id_proveedor}?token=${token}`)
						  .map(data => {
							 return data.json();
						  })
  }

  update(form:ProveedorInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/proveedor?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }
}
