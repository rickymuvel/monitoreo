import { Component } from '@angular/core';
import { Message, SelectItem } from 'primeng/components/common/api';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { MatrizPonderacionesInterface } from '../../interfaces/matriz-ponderaciones.interface';
import { ReaccionInterface } from '../../interfaces/reaccion.interface';

import { NegociosInterface } from '../../interfaces/negocio.interface';
import { CampanasInterface } from '../../interfaces/campana.interface';
import { ProveedorInterface } from '../../interfaces/proveedor.interface';
import { CarterasInterface } from '../../interfaces/cartera.interface';

import { MatrizPonderacionesService } from '../../servicios/matriz-ponderaciones.service';
import { ReaccionesService } from '../../servicios/reacciones.service';


import { NegociosService } from '../../servicios/negocios.service';
import { CampanasService } from '../../servicios/campanas.service';
import { ProveedorService } from '../../servicios/proveedor.service';
import { CarteraService } from '../../servicios/cartera.service';

@Component({
	selector: 'app-matriz-ponderaciones',
	templateUrl: './matriz-ponderaciones.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class MatrizPonderacionesComponent {

	msjs:Message[] = [];
	accion:string = "agregar";
	total_registros:string = "";

	MatrizPonderaciones:MatrizPonderacionesInterface[] = [];
	Reacciones:ReaccionInterface[] = [];

	Negocios:NegociosInterface[] = [];
	Negocios_unicos:SelectItem[] = [];

	Campanas:CampanasInterface[] = [];
	Campanas_unicas:SelectItem[] = [];

	Proveedores:ProveedorInterface[] = [];
	Proveedores_filtro:ProveedorInterface[] = [];
	Proveedores_unicos:SelectItem[];

	Carteras:CarterasInterface[] = [];
	Carteras_filtro:CarterasInterface[] = [];
	Carteras_unicas:SelectItem[] = [];

	display_modal:boolean = false;
	Formulario:FormGroup;

	MatrizPonderacionesSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;
	edit_disabled:boolean = true;
	primera_carga_web = false;

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){}
	}

	constructor(
		private _mps:MatrizPonderacionesService,
		private _rs:ReaccionesService,
		private _ns:NegociosService,
		private _cas:CampanasService,
		private _pvs:ProveedorService,
		private _ctra:CarteraService,
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'reaccion': new FormControl(this.objeto.reaccion),
			'descripcion': new FormControl(this.objeto.descripcion),
			'negocio': new FormControl(this.objeto.negocio),
			'campana': new FormControl(this.objeto.campana),
			'proveedor': new FormControl(this.objeto.proveedor),
			'cartera': new FormControl(this.objeto.cartera),
			'id_reaccion': new FormControl(this.objeto.id_reaccion, Validators.required),
			'id_negocio': new FormControl(this.objeto.id_negocio, Validators.required),
			'id_cartera': new FormControl(this.objeto.id_cartera, Validators.required),
			'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
			'id_campana': new FormControl(this.objeto.id_campana, Validators.required),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto_filtro:any = {
		id_negocio: "",
		id_campana: "",
		id_proveedor: "",
		id_cartera: ""
	}

	objeto:MatrizPonderacionesInterface = {
			id: '',
			id_reaccion: '',
			reaccion: '',
			descripcion: '',
			negocio: '',
			campana: '',
			proveedor: '',
			cartera: '',
			id_negocio: '',
			id_cartera: '',
			id_proveedor: '',
			id_campana: '',
			estado: ''
	}
	objeto_reset:MatrizPonderacionesInterface = {
			id: '',
			id_reaccion: '',
			reaccion: '',
			descripcion: '',
			negocio: '',
			campana: '',
			proveedor: '',
			cartera: '',
			id_negocio: '',
			id_cartera: '',
			id_proveedor: '',
			id_campana: '',
			estado: ''
		}

	handleMatrizPonderacionesRowSelect(obj){
		if(this.MatrizPonderacionesSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
	}

	crearModal(){
		this.display_modal = true;
		this.Formulario.controls["id_reaccion"].enable();
		this.Formulario.controls["id_negocio"].enable();
		this.Formulario.controls["id_cartera"].enable();
		this.Formulario.controls["id_proveedor"].enable();
		this.Formulario.controls["id_campana"].enable();
		this._rs.get()
						.subscribe(data => {
							this.Reacciones = data.data;
						});
		// this._mps.get()
		// 				.subscribe(data => {
		// 					this.display_modal = true;
		// 					this.Portafolios = [];
		// 					this.Portafolios = data.data;
		// 					setTimeout(()=>{
		// 						$("#campana").focus();
		// 					},500);
		// 				});
	}

	obtenerDetalles(id_reaccion){
		let descripcion = "";
		this.Reacciones.forEach((obj)=>{
			if(obj.id==id_reaccion){
				descripcion = obj.descripcion;
			}
		});
		this.objeto.descripcion = descripcion;
		// this._pors.getDetalles(id_portafolio)
		// 					.subscribe(data => {
		// 						this.objeto.id_campana = data.data[0].id_campana;
		// 						this.objeto.campana = data.data[0].campana;
		// 						this.objeto.id_negocio = data.data[0].id_negocio;
		// 						this.objeto.negocio = data.data[0].negocio;
		// 						this.objeto.id_proveedor = data.data[0].id_proveedor;
		// 						this.objeto.proveedor = data.data[0].proveedor;
		// 						this.objeto.id_cartera = data.data[0].id_cartera;
		// 						this.objeto.cartera = data.data[0].cartera;
		// 						this.objeto.base = data.data[0].base;
		// 					});
	}

	editarModal( dt ){
		this.Formulario.controls["id_reaccion"].disable();
		this.Formulario.controls["id_negocio"].disable();
		this.Formulario.controls["id_cartera"].disable();
		this.Formulario.controls["id_proveedor"].disable();
		this.Formulario.controls["id_campana"].disable();
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.display_modal = true;
		this.edit_disabled = true;

		this._rs.get()
						.subscribe(data => {
							this.Reacciones = data.data;
						},
						error => {
							this._rs.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		// this._pors.get()
		// 					.subscribe(data=>{
		// 						// debugger;
		// 						this.Portafolios = data.data;
		// 						let obj = this.Clonar(dt.selection);
		// 						this.Formulario.setValue(obj);
		// 						this.display_modal = true;
		// 					});
	}

	Clonar(r: MatrizPonderacionesInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	filtrarMatrizPonderaciones(){
		this._mps.get(
			this.objeto_filtro.id_negocio,
			this.objeto_filtro.id_campana,
			this.objeto_filtro.id_proveedor,
			this.objeto_filtro.id_cartera
			)
				.subscribe(data => {
					this.MatrizPonderaciones = [];
					this.MatrizPonderaciones = data.data;
					this.total_registros = `${data.data.length} registros encontrados`;
				},
				error => {
					this._mps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	get(){
		if(this.primera_carga_web){
			this.filtrarMatrizPonderaciones();
		}
		// esta variable permite que el método filtrarMatrizPonderaciones() se ejecute a partir de la segunda vez
		// la primera vez no se ejecutará
		this.primera_carga_web = true; 

		// this._cs.get()
		// 		.subscribe(data => {
		// 			this.Clasificaciones = [];
		// 			this.Clasificaciones = data.data;
		// 		});
		// this._as.get()
		// 		.subscribe(data => {
		// 			this.Atributos = [];
		// 			this.Atributos = data.data;
		// 		});
		// this._ts.get()
		// 		.subscribe(data => {
		// 			this.TipoError = [];
		// 			this.TipoError = data.data;
		// 		});
		// this._ces.get()
		// 		.subscribe(data => {
		// 			this.Criterio = [];
		// 			this.Criterio = data.data;
		// 		});

		this._ns.getNegociosUnicos()
						.subscribe(data => {
							this.Negocios_unicos = [];
							this.Negocios_unicos = data.data;
						},
						error =>{
							this._ns.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		this._ns.get()
						.subscribe(data=>{
							this.Negocios = [];
							this.Negocios = data.data;
						},
						error =>{
							let err = error.json();
							this.handleErrorManager(err);
						});

		this._cas.getCampanasUnicas()
						.subscribe(data => {
							this.Campanas_unicas = [];
							this.Campanas_unicas = data.data;
						},
						error =>{
							this._cas.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		this._cas.get()
						.subscribe(data=>{
							this.Campanas = [];
							this.Campanas = data.data;
						},
						error =>{
							this._cas.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});

		this._pvs.getProveedoresUnicos()
						.subscribe(data => {
							this.Proveedores_unicos = [];
							this.Proveedores_unicos = data.data;
						},
						error =>{
							this._pvs.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		this._pvs.get()
						.subscribe(data=>{
							this.Proveedores = [];
							this.Proveedores = data.data;
						},
						error =>{
							this._pvs.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});

		this._ctra.getCarterasUnicas()
						.subscribe(data => {
							this.Carteras_unicas = [];
							this.Carteras_unicas = data.data;
						},
						error =>{
							this._ctra.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
						
		this._ctra.get()
						.subscribe(data=>{
							this.Carteras = [];
							console.log(data.data);
							this.Carteras = data.data;
						},
						error =>{
							this._ctra.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
	}

	set(){
		this._mps.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.filtrarMatrizPonderaciones();
					// this.MatrizPonderaciones = [];
					// this.MatrizPonderaciones = data.data;
					this.display_modal = false;
				}, 
				error => {
					this._mps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._mps.update( this.objeto )
						.subscribe(data => {
							this.showSuccess( "Exito", "Hemos actualizado el registro" );
							this.get();
						},
						error => {
							this._mps.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	getCarterasxProveedor(id_proveedor, procedencia="filtro"){
		this._pvs.getCarterasxProveedor(id_proveedor)
							.subscribe(data =>{
								if(procedencia=="filtro"){
									this.Carteras_filtro = [];
									this.Carteras_filtro = data.data
								}
								else{
									this.Carteras = [];
									this.Carteras = data.data;
								}
							},
							error =>{
								this._pvs.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
							})
	}

	getProveedoresxCarteras(id_campana){
		this._cas.getProveedoresxCampana(id_campana)
						 .subscribe(data =>{
						 	this.Proveedores_filtro = [];
						 	this.Proveedores_filtro = data.data;
						 },
						 error => {
						 	this._pvs.handleError(error);
						 	let err = error.json();
							this.handleErrorManager(err);
						 })
	}

	estadoFiltro(){
		if(this.objeto_filtro.id_negocio!="" && 
				this.objeto_filtro.id_campana!="" && 
				this.objeto_filtro.id_proveedor!="" && 
				this.objeto_filtro.id_cartera!=""){
			return true;
		}else{
			return false;
		}

	}

	showSuccess( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

	clear(){
		this.msjs = [];
	}

}

class ClonRegistro implements MatrizPonderacionesInterface {
	 constructor(
		public id?,
		public reaccion?,
		public descripcion?,
		public negocio?,
		public campana?,
		public proveedor?,
		public id_negocio?,
		public id_cartera?,
		public id_proveedor?,
		public id_campana?,
		public cartera?){}
}