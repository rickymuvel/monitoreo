import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { UsuariosInterface } from '../interfaces/usuarios.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

import 'rxjs/add/operator/map';


@Injectable()
export class UsuarioService {
  constructor(
    private sett:Settings,
    private _ls:LoginService,
    private _es:ErrorService,
		private http:Http){}

  set(form:UsuariosInterface){
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/usuarios?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  })
  }

  get(){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/usuarios?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  getCTQ(form:any){
   let token = this._ls.getToken();
   let body = JSON.stringify(form);
   let headers = new Headers({
    'Content-Type': 'application/json'
   });

   return this.http.post(`${this.sett.url}/tipo-error/ctq?token=${token}`, body, { headers })
              .map(data => {
               return data.json();
              })
  }

  getJefeCuenta(){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/usuarios/jefe-cuenta?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  getAgentes(id_monitor){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/usuarios/agentes/${id_monitor}?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  // solo debe traer a los evaluadores con estado finalizado.
  getEvaluadores(id_historico){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/usuarios/evaluadores/${id_historico}?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  update(form:UsuariosInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/usuarios?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }

}
