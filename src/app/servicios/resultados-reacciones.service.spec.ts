import { TestBed, inject } from '@angular/core/testing';

import { ResultadosReaccionesService } from './resultados-reacciones.service';

describe('ResultadosReaccionesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResultadosReaccionesService]
    });
  });

  it('should be created', inject([ResultadosReaccionesService], (service: ResultadosReaccionesService) => {
    expect(service).toBeTruthy();
  }));
});
