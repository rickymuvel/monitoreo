import { Injectable } from '@angular/core';

@Injectable()
export class Settings {
  url:string =  "";
  email:string = "";
  constructor(){
    // this.url = 'http://20.200.70.53/api';
    this.url = 'http://monitoreo.local/api';
    // this.url = 'http://monitoreo.net/api';
    // this.url = 'http://192.168.30.156/api';
    this.email = 'lmunoz@kobsa.com.pe';
  }
}
