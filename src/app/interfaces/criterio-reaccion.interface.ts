export class CriterioReaccionInterface {
	id?:string;
	id_reaccion?:string;
	reaccion?:string;
	id_criterio?:string;
	criterio?:string;
	prioridad?:string;
	peso?:string;
	estado?:string;
}
