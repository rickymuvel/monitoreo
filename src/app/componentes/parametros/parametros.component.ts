import { Component } from '@angular/core';
// import { Message, SelectItem } from 'primeng/primeng';
import { Message, SelectItem } from 'primeng/components/common/api';
// import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { ParametroInterface } from '../../interfaces/parametro.interface';
import { ClasificacionInterface } from '../../interfaces/clasificacion.interface';
import { AtributoInterface } from '../../interfaces/atributo.interface';
import { TipoDeErrorInterface } from '../../interfaces/tipo-de-error.interface';
import { CriterioEvaluacionInterface } from '../../interfaces/criterio-evaluacion.interface';
import { PortafoliosInterface } from '../../interfaces/portafolio.interface';


import { NegociosInterface } from '../../interfaces/negocio.interface';
import { CampanasInterface } from '../../interfaces/campana.interface';
import { ProveedorInterface } from '../../interfaces/proveedor.interface';
import { CarterasInterface } from '../../interfaces/cartera.interface';

import { ParametrosService } from '../../servicios/parametros.service';
import { ClasificacionesService } from '../../servicios/clasificaciones.service';
import { AtributosService } from '../../servicios/atributos.service';
import { TipoDeErrorService } from '../../servicios/tipo-de-error.service';
import { CriterioEvaluacionService } from '../../servicios/criterio-evaluacion.service';
import { PortafolioService } from '../../servicios/portafolio.service';

import { NegociosService } from '../../servicios/negocios.service';
import { CampanasService } from '../../servicios/campanas.service';
import { ProveedorService } from '../../servicios/proveedor.service';
import { CarteraService } from '../../servicios/cartera.service';

@Component({
	selector: 'app-parametros',
	templateUrl: './parametros.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ParametrosComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	Parametros:ParametroInterface[] = [];
	Clasificaciones:ClasificacionInterface[] = [];
	Atributos:AtributoInterface[] = [];
	TipoError:TipoDeErrorInterface[] = [];
	Criterio:CriterioEvaluacionInterface[] = [];

	Negocios:NegociosInterface[] = [];
	Negocios_unicos:SelectItem[] = [];

	Campanas:CampanasInterface[] = [];
	Campanas_unicas:SelectItem[] = [];

	Proveedores:ProveedorInterface[] = [];
	Proveedores_filtro:ProveedorInterface[] = [];
	Proveedores_unicos:SelectItem[];

	Carteras:CarterasInterface[] = [];
	Carteras_filtro:CarterasInterface[] = [];
	Carteras_unicas:SelectItem[] = [];

	Portafolios:PortafoliosInterface[] = [];


	display_modal:boolean = false;
	Formulario:FormGroup;
	parametrosSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;
	primera_carga_web = false;

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){}
	}

	constructor(
		private _ps:ParametrosService,
		private _cs:ClasificacionesService,
		private _as:AtributosService,
		private _ts:TipoDeErrorService,
		private _ces:CriterioEvaluacionService,
		private _ns:NegociosService,
		private _cas:CampanasService,
		private _pvs:ProveedorService,
		private _ctra:CarteraService,
		private _pors:PortafolioService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'id_portafolio': new FormControl(this.objeto.id_portafolio, Validators.required),
			'id_negocio': new FormControl(this.objeto.id_negocio),
			'negocio': new FormControl(this.objeto.negocio),
			'id_proveedor': new FormControl(this.objeto.id_proveedor),
			'proveedor': new FormControl(this.objeto.proveedor),
			'id_campana': new FormControl(this.objeto.id_campana, Validators.required),
			'campana': new FormControl(this.objeto.campana),
			'id_cartera': new FormControl(this.objeto.id_cartera),
			'cartera': new FormControl(this.objeto.cartera),
			'base': new FormControl(this.objeto.base),
			'id_clasificacion': new FormControl(this.objeto.id_clasificacion, Validators.required),
			'clasificacion': new FormControl(this.objeto.clasificacion),
			'id_atributo': new FormControl(this.objeto.id_atributo, Validators.required),
			'atributo': new FormControl(this.objeto.atributo),
			'id_tipo_error': new FormControl(this.objeto.id_tipo_error, Validators.required),
			'descripcion': new FormControl(this.objeto.descripcion),
			'id_criterio': new FormControl(this.objeto.id_criterio, Validators.required),
			'criterio': new FormControl(this.objeto.criterio),
			'peso': new FormControl(this.objeto.peso),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto_filtro:any = {
		id_negocio: "",
		id_campana: "",
		id_proveedor: "",
		id_cartera: ""
	}

	objeto:ParametroInterface = {
			id: "",
			base: "",
			id_portafolio: "",
			id_negocio: "",
			negocio: "",
			id_proveedor: "",
			proveedor: "",
			id_campana: "",
			campana: "",
			id_cartera: "",
			cartera: "",
			id_clasificacion: "",
			clasificacion: "",
			id_atributo: "",
			atributo: "",
			id_tipo_error: "",
			descripcion: "",
			id_criterio: "",
			criterio: "",
			peso: "",
			estado: ""
	}
	objeto_reset:ParametroInterface = {
			id: "",
			id_portafolio: "",
			id_negocio: "",
			negocio: "",
			id_proveedor: "",
			proveedor: "",
			id_campana: "",
			campana: "",
			id_cartera: "",
			cartera: "",
			base: "",
			id_clasificacion: "",
			clasificacion: "",
			id_atributo: "",
			atributo: "",
			id_tipo_error: "",
			descripcion: "",
			id_criterio: "",
			criterio: "",
			peso: "",
			estado: ""
		}

	handleParametrosRowSelect(obj){
		if(this.parametrosSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
	}

	crearModal(){
		this._pors.get()
						.subscribe(data => {
							this.display_modal = true;
							this.Portafolios = [];
							this.Portafolios = data.data;
							setTimeout(()=>{
								$("#campana").focus();
							},500);
						},
						error => {
							this._pors.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
	}

	obtenerDetalles(id_portafolio){
		this._pors.getDetalles(id_portafolio)
							.subscribe(data => {
								this.objeto.id_campana = data.data[0].id_campana;
								this.objeto.campana = data.data[0].campana;
								this.objeto.id_negocio = data.data[0].id_negocio;
								this.objeto.negocio = data.data[0].negocio;
								this.objeto.id_proveedor = data.data[0].id_proveedor;
								this.objeto.proveedor = data.data[0].proveedor;
								this.objeto.id_cartera = data.data[0].id_cartera;
								this.objeto.cartera = data.data[0].cartera;
								this.objeto.base = data.data[0].base;
							},
							error => {
								this._pors.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
							});
	}

	editarModal( dt ){
		this.accion = "editar";
		this._pors.get()
							.subscribe(data=>{
								// debugger;
								this.Portafolios = data.data;
								let obj = this.Clonar(dt.selection);
								this.Formulario.setValue(obj);
								this.display_modal = true;
							},
							error => {
								this._pors.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
							});
	}

	Clonar(r: ParametroInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	filtrarParametros(){
		this._ps.get(
			this.objeto_filtro.id_negocio,
			this.objeto_filtro.id_campana,
			this.objeto_filtro.id_proveedor,
			this.objeto_filtro.id_cartera
			)
				.subscribe(data => {
					this.Parametros = [];
					this.Parametros = data.data;
					this.Portafolios = [];
					if(data.data.length>0){
						this.csv = true;
					}
					else{
						this.csv = false;
						this.showWarn("Sin resultados", "Lo lamento, pero no se obtuvieron resultados");
					}
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	get(){
		if(this.primera_carga_web){
			this.filtrarParametros();
		}
		// esta variable permite que el método filtrarParametros() se ejecute a partir de la segunda vez
		// la primera vez no se ejecutará
		this.primera_carga_web = true; 

		this._cs.get()
				.subscribe(data => {
					this.Clasificaciones = [];
					this.Clasificaciones = data.data;
				},
				error => {
					this._cs.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		this._as.get()
				.subscribe(data => {
					this.Atributos = [];
					this.Atributos = data.data;
				},
				error => {
					this._as.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		this._ts.get()
				.subscribe(data => {
					this.TipoError = [];
					this.TipoError = data.data;
				},
				error => {
					this._ts.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		this._ces.get()
				.subscribe(data => {
					this.Criterio = [];
					this.Criterio = data.data;
				},
				error => {
					this._ces.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});

		this._ns.getNegociosUnicos()
						.subscribe(data => {
							this.Negocios_unicos = [];
							this.Negocios_unicos = data.data;
						},
						error =>{
							this._ns.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		this._ns.get()
						.subscribe(data=>{
							this.Negocios = [];
							this.Negocios = data.data;
						},
						error =>{
							this._ns.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});

		this._cas.getCampanasUnicas()
						.subscribe(data => {
							this.Campanas_unicas = [];
							this.Campanas_unicas = data.data;
						},
						error =>{
							this._cas.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		this._cas.get()
						.subscribe(data=>{
							this.Campanas = [];
							this.Campanas = data.data;
						},
						error =>{
							this._cas.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});

		this._pvs.getProveedoresUnicos()
						.subscribe(data => {
							this.Proveedores_unicos = [];
							this.Proveedores_unicos = data.data;
						},
						error =>{
							this._pvs.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		this._pvs.get()
						.subscribe(data=>{
							this.Proveedores = [];
							this.Proveedores = data.data;
						},
						error =>{
							this._pvs.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});

		this._ctra.getCarterasUnicas()
						.subscribe(data => {
							this.Carteras_unicas = [];
							this.Carteras_unicas = data.data;
						},
						error => {
							this._ctra.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
		this._ctra.get()
						.subscribe(data=>{
							this.Carteras = [];
							this.Carteras = data.data;
						},
						error =>{
							this._ctra.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
	}

	set(){
		this._ps.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.Parametros = [];
					this.Parametros = data.data;
					this.display_modal = false;
				}, 
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._ps.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._ps.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	getCarterasxProveedor(id_proveedor){
		this._pvs.getCarterasxProveedor(id_proveedor)
							.subscribe(data =>{
								this.Carteras_filtro = [];
								this.Carteras_filtro = data.data
							},
							error =>{
								this._pvs.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
							})
	}

	getProveedoresxCarteras(id_campana){
		this._cas.getProveedoresxCampana(id_campana)
						 .subscribe(data =>{
						 	this.Proveedores_filtro = [];
						 	this.Proveedores_filtro = data.data;
						 },
						 error => {
						 	this._cas.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
						 })
	}

	estadoFiltro(){
		if(this.objeto_filtro.id_negocio!="" && 
				this.objeto_filtro.id_campana!="" && 
				this.objeto_filtro.id_proveedor!="" && 
				this.objeto_filtro.id_cartera!=""){
			return true;
		}else{
			return false;
		}

	}

	showSuccess( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

	clear(){
		this.msjs = [];
	}

}

class ClonRegistro implements ParametroInterface {
	 constructor(
		public id?,
		public id_negocio?,
		public negocio?,
		public id_proveedor?,
		public proveedor?,
		public id_campana?,
		public campana?,
		public id_cartera?,
		public cartera?,
		public base?,
		public clasificacion?,
		public atributo?,
		public descripcion?,
		public criterio?,
		public peso?,
		public estado?){}
}