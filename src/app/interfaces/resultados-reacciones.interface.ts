export class ResultadosReaccionesInterface {
	id?:number;
	actividad?:string;
	resultado?:string;
	justificacion?:string;
	contactabilidad?:string;
	reaccion?:string;
	id_reaccion?:number;
	negocio?:string;
	proveedor?:string;
	cartera?:string;
	estado?:string;
}