import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { TipoDeErrorInterface } from '../interfaces/tipo-de-error.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'
// import { TipoProductoInterface } from '../interfaces/tipo-producto.interface';
// import { ProductoInterface } from '../interfaces/producto.interface';
import 'rxjs/add/operator/map';


@Injectable()
export class TipoDeErrorService {
  constructor(
    private sett:Settings,
    private _ls:LoginService,
    private _es:ErrorService,
  	private http:Http) { }

  set(form:TipoDeErrorInterface){
	   let token = this._ls.getToken();
  	 let body = JSON.stringify(form);
  	 let headers = new Headers({
  		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/tipo-error?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  })
  }

  get(){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/tipo-error?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  // getCTQ(){
	 // // let token = this._ls.getToken();
	 // let token = "";
	 // return this.http.get(`${this.sett.url}/tipo-error/ctq?token=${token}`)
		// 				  .map(data => {
		// 					 return data.json();
		// 				  })
  // }

  getCTQ(form:any){
     let token = this._ls.getToken();
     let body = JSON.stringify(form);
     let headers = new Headers({
      'Content-Type': 'application/json'
     });

   return this.http.post(`${this.sett.url}/tipo-error/ctq?token=${token}`, body, { headers })
              .map(data => {
               return data.json();
              })
  }

  update(form:TipoDeErrorInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/tipo-error?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }


}
