export class EvaluacionesInterface {
	id?:string;
	documento?:string;
	cliente?:string;
	telefono?:string;
	fecha?:string;
	negocio?:string;
	status_audio?:string;
	nota?:string;
	comentarios?:string;
	validar_email?:string;
}
