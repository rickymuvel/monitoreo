import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { DatePipe } from '@angular/common';
declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { UsuariosInterface } from '../../interfaces/usuarios.interface';
import { ProveedorInterface } from '../../interfaces/proveedor.interface';
import { CarterasInterface } from '../../interfaces/cartera.interface';

import { UsuarioService } from '../../servicios/usuario.service';
import { ProveedorService } from '../../servicios/proveedor.service';
import { CarteraService } from '../../servicios/cartera.service';

@Component({
	selector: 'app-usuarios',
	templateUrl: './usuarios.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class UsuariosComponent {

	msjs:Message[] = [];
	accion:string = "agregar";
	Usuarios:UsuariosInterface[] = [];
	JefeCuenta:UsuariosInterface[] = [];
	Proveedores:ProveedorInterface[] = [];
	Carteras:CarterasInterface[] = [];

	display_modal:boolean = false;
	Formulario:FormGroup;
	usuariosSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){
		}
	}

	transformDate(date) {
			this.datePipe.transform(date, 'yyyy-MM-dd');
		}

	constructor(
		private _us:UsuarioService,
		private _ps:ProveedorService,
		private _cs:CarteraService,
		private datePipe: DatePipe
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'usuario': new FormControl(this.objeto.usuario, Validators.required),
			'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
			'proveedor': new FormControl(this.objeto.proveedor, Validators.required),
			'id_cartera': new FormControl(this.objeto.id_cartera, Validators.required),
			'cartera': new FormControl(this.objeto.cartera, Validators.required),
			'fecha_ingreso_kobsa': new FormControl(this.objeto.fecha_ingreso_kobsa, Validators.required),
			'fecha_ingreso_portafolio': new FormControl(this.objeto.fecha_ingreso_portafolio, Validators.required),
			'condicion_kobsa': new FormControl(this.objeto.condicion_kobsa, Validators.required),
			'condicion_portafolio': new FormControl(this.objeto.condicion_portafolio, Validators.required),
			'id_jefe_cuenta': new FormControl(this.objeto.id_jefe_cuenta, Validators.required),
			'jefe_cuenta': new FormControl(this.objeto.jefe_cuenta, Validators.required),
			'turno': new FormControl(this.objeto.turno, Validators.required),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto:UsuariosInterface = {
			id: "",
			usuario: "",
			id_proveedor: "",
			proveedor: "",
			id_cartera: "",
			cartera: "",
			fecha_ingreso_kobsa: "",
			fecha_ingreso_portafolio: "",
			condicion_kobsa: "",
			condicion_portafolio: "",
			id_jefe_cuenta: "",
			jefe_cuenta: "",
			turno: "",
			estado: ""
		}

		objeto_reset:UsuariosInterface = {
			id: "",
			usuario: "",
			id_proveedor: "",
			proveedor: "",
			id_cartera: "",
			cartera: "",
			fecha_ingreso_kobsa: "",
			fecha_ingreso_portafolio: "",
			condicion_kobsa: "",
			condicion_portafolio: "",
			id_jefe_cuenta: "",
			jefe_cuenta: "",
			turno: "",
			estado: ""
		}

		handleUsuariosRowSelect(obj){
			if(this.usuariosSelected==null){
				this.can_edit = false;
			}
			else{
				this.can_edit = true;
			}
		}

	get(){
		this._us.get()
				.subscribe(data => {
					this.Usuarios = [];
					this.Usuarios = data.data;
					if(data.data.length>0){
						this.csv = true;
					}
					else{
						this.csv = false;
					}
				},
				error =>{
					this._us.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		this._ps.get()
				.subscribe(data => {
					this.Proveedores = [];
					this.Proveedores = data.data;
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	buscarCarteras(id_proveedor){
		this._ps.getCarterasxProveedor(id_proveedor)
				.subscribe(data => {
					this.Carteras = [];
					this.Carteras = data.data;
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	};

	set(){
		this._us.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.Usuarios = [];
					this.Usuarios = data.data;
					this.display_modal = false;
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	editarModal( dt ){
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this._ps.getCarterasxProveedor(dt.selection.id_proveedor)
				.subscribe(data => {
					this.Carteras = [];
					this.Carteras = data.data;
					this._us.getJefeCuenta()
									.subscribe(data => {
										this.JefeCuenta = data.data;
										this.Formulario.setValue(obj);
										this.display_modal = true;
										let ingreso_kobsa = this.objeto.fecha_ingreso_kobsa.split(" ")[0].replace(/-/gi, "/");
										let ingreso_portafolio = this.objeto.fecha_ingreso_portafolio.split(" ")[0].replace(/-/gi, "/");
										$('#fecha_ingreso_kobsa').datetimepicker({
											useCurrent: false,
											defaultDate: dt.selection.fecha_ingreso_kobsa
										});
										$('#fecha_ingreso_portafolio').datetimepicker();
									});
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		
	}

	Clonar(r: UsuariosInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._us.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._us.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
		this.showMultiple([
			{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
			{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
		]);
	}

	clear(){
		this.msjs = [];
	}

}

class ClonRegistro implements UsuariosInterface {
	 constructor(
	public id?,
	public usuario?,
	public id_proveedor?,
	public proveedor?,
	public id_cartera?,
	public cartera?,
	public fecha_ingreso_kobsa?,
	public fecha_ingreso_portafolio?,
	public condicion_kobsa?,
	public condicion_portafolio?,
	public id_jefe_cuenta?,
	public jefe_cuenta?,
	public turno?,
	public estado?){}
}