import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { ReaccionInterface } from '../interfaces/reaccion.interface';
import { LoginService } from './login.service';
import { ErrorService } from './error.service';

import 'rxjs/add/operator/map';


@Injectable()
export class ReaccionesService {
  constructor(
      private sett:Settings,
      private _es:ErrorService,
      private _ls:LoginService,
			private http:Http) { }

  set( form:ReaccionInterface ) {
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/reacciones?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  })
  }

  get(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/reacciones?token=${token}`)
						  .map(data => {
							 return data.json();
						  })
  }

  update(form:ReaccionInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/reacciones?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }

}
