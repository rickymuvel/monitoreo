import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { TipoDeErrorAtributoInterface} from '../interfaces/tipo-de-error-atributo.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'
// import { TipoProductoInterface } from '../interfaces/tipo-producto.interface';
// import { ProductoInterface } from '../interfaces/producto.interface';
import 'rxjs/add/operator/map';


@Injectable()
export class TipoDeErrorAtributoService {
  constructor(private sett:Settings,
      private _ls:LoginService,
      private _es:ErrorService,
      private http:Http) { }

  set(form:TipoDeErrorAtributoInterface){
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/tipo-error-atributo?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  })
  }

  get(){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/tipo-error-atributo?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  update(form:TipoDeErrorAtributoInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/tipo-error-atributo?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }

}
