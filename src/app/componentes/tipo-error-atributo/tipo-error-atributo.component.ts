import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { TipoDeErrorAtributoInterface } from '../../interfaces/tipo-de-error-atributo.interface';
import { TipoDeErrorAtributoService } from '../../servicios/tipo-de-error-atributo.service';
import { TipoDeErrorService } from '../../servicios/tipo-de-error.service';
import { AtributosService } from '../../servicios/atributos.service';

@Component({
  selector: 'app-tipo-error-atributo',
  templateUrl: './tipo-error-atributo.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class TipoErrorAtributoComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){
		}
	}

	TiposDeErrorAtributo:TipoDeErrorAtributoInterface[] = [];
	TiposErrores:any[] = [];
	Atributos:any[] = [];

	display_modal:boolean = false;
	Formulario:FormGroup;
	TiposDeErrorAtributoSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	constructor(
		private _teas:TipoDeErrorAtributoService,
		private _tes:TipoDeErrorService,
		private _as:AtributosService
		) {
		this.Formulario = new FormGroup({
	      'id': new FormControl(this.objeto.id),
	      'id_atributo': new FormControl(this.objeto.id_atributo, Validators.required),
	      'atributo': new FormControl(this.objeto.atributo, Validators.required),
	      'id_tipo_error': new FormControl(this.objeto.id_tipo_error),
	      'tipo_error': new FormControl(this.objeto.tipo_error),
	      'ctq': new FormControl(this.objeto.ctq),
	      'estado': new FormControl(this.objeto.estado, Validators.required)
	    });
	    this.get();
	}

	getCTQ(te){
		let obj = {id_tipo_error: te};
		this._tes.getCTQ(obj)
				.subscribe(data=>{
					this.objeto.ctq = data.data[0].ctq;
				},
				error => {
					this._tes.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	objeto:TipoDeErrorAtributoInterface = {
	    id:'',
			id_atributo:'',
			atributo:'',
			id_tipo_error:'',
			tipo_error:'',
			ctq:'',
			estado:''
  	}

  	objeto_reset:TipoDeErrorAtributoInterface = {
	    id:'',
			id_atributo:'',
			atributo:'',
			id_tipo_error:'',
			tipo_error:'',
			ctq:'',
			estado:''
  	}

  	handleTiposDeErrorAtributoRowSelect(obj){
	    if(this.TiposDeErrorAtributoSelected==null){
	      this.can_edit = false;
	    }
	    else{
	      this.can_edit = true;
	    }
	  }

	  crearModal(){
	    this.display_modal = true;
	  }

	  get(){
	    this._teas.get()
	        .subscribe(data => {
	          this.TiposDeErrorAtributo = [];
	          this.TiposDeErrorAtributo = data.data;
	          if(data.data.length>0){
	            this.csv = true;
	          }
	          else{
	            this.csv = false;
	          }
	        },
	        error => {
	        	this._teas.handleError(error);
						let err = error.json();
						this.handleErrorManager(err);
	        });
	    // cargamos los tipos de errores
	    this._tes.get()
	        .subscribe(data => {
	          this.TiposErrores = [];
	          this.TiposErrores = data.data;
	        },
	        error => {
	        	this._tes.handleError(error);
						let err = error.json();
						this.handleErrorManager(err);
	        });
	    // cargamos los atributos
	    this._as.get()
	        .subscribe(data => {
	          this.Atributos = [];
	          this.Atributos = data.data;
	        },
	        error => {
	        	this._as.handleError(error);
						let err = error.json();
						this.handleErrorManager(err);
	        });
	  }
	  

	  set(){
	    this._teas.set(this.objeto)
	        .subscribe(data =>{
	          this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
	          this.TiposDeErrorAtributo = [];
	          this.TiposDeErrorAtributo = data.data;
	          this.display_modal = false;
	        },
	        error => {
	        	this._teas.handleError(error);
						let err = error.json();
						this.handleErrorManager(err);
	        });
	  }

	  editarModal( dt ){
			this.accion = "editar";
			let obj = this.Clonar(dt.selection);
			this.Formulario.setValue(obj);
			this.display_modal = true;
		}

		Clonar(r: TipoDeErrorAtributoInterface){
			let objeto = new ClonRegistro();
			for(let prop in r){
				if(this.objeto.hasOwnProperty(prop)){
					objeto[prop] = r[prop];
				}
			}
			return objeto;
		}

		update(){ // vendría a ser Editar(){}
			this.display_modal = false;
			this._teas.update( this.objeto )
			.subscribe(data => {
				this.showSuccess( "Exito", "Hemos actualizado el registro" );
				this.get();
			},
			error => {
				this._teas.handleError(error);
				let err = error.json();
				this.handleErrorManager(err);
			});
		}

		cambiarEstado(){
			this.accion = "agregar";
			this.Formulario.setValue(this.objeto_reset);
		}

		showSuccess( header, mensaje ) {
			this.msjs.push({severity:'success', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showInfo( header, mensaje ) {
			this.msjs.push({severity:'info', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showWarn( header, mensaje ) {
			this.msjs.push({severity:'warn', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showError( header, mensaje ) {
			this.msjs.push({severity:'error', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showMultiple(arreglo:any[]) {
			this.msjs = [];
			for (var i = 0; i < arreglo.length; i++) {
				this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
			}
			setTimeout(()=> this.clear(), 3000);
		}

		handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

		clear(){
			this.msjs = [];
		}

}

class ClonRegistro implements TipoDeErrorAtributoInterface {
   constructor(
	public id?,
	public id_atributo?,
	public atributo?,
	public id_tipo_error?,
	public tipo_error?,
	public ctq?,
	public estado?){}
}