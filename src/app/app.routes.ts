import { RouterModule, Routes } from '@angular/router';

import { NegociosComponent } from './componentes/negocios/negocios.component';
import { CampanaComponent } from './componentes/campana/campana.component';
import { PortafolioComponent } from './componentes/portafolio/portafolio.component';
import { ReaccionComponent } from './componentes/reaccion/reaccion.component';
import { UsuariosComponent } from './componentes/usuarios/usuarios.component';
import { ClasificacionComponent } from './componentes/clasificacion/clasificacion.component';
import { AtributoComponent } from './componentes/atributo/atributo.component';
// import { AtributoClasificacionComponent } from './componentes/atributo-clasificacion/atributo-clasificacion.component';
import { TipoDeErrorComponent } from './componentes/tipo-de-error/tipo-de-error.component';
// import { TipoErrorAtributoComponent } from './componentes/tipo-error-atributo/tipo-error-atributo.component';
import { CriteriosEvaluacionComponent } from './componentes/criterios-evaluacion/criterios-evaluacion.component';
import { ResultadosReaccionesComponent } from './componentes/resultados-reacciones/resultados-reacciones.component';
import { ArliComponent } from './componentes/negocios/arli.component';
import { CriteriosReaccionComponent } from './componentes/criterios-reaccion/criterios-reaccion.component';
import { AsignarEvaluacionesComponent } from './componentes/asignar-evaluaciones/asignar-evaluaciones.component';
import { ParametrosComponent } from './componentes/parametros/parametros.component';
import { EvaluacionesComponent } from './componentes/evaluaciones/evaluaciones.component';
import { MatrizPonderacionesComponent } from './componentes/matriz-ponderaciones/matriz-ponderaciones.component';

const APP_ROUTES: Routes = [
  { path: 'negocios', component: NegociosComponent },
  { path: 'campanas', component: CampanaComponent },
  { path: 'portafolios', component: PortafolioComponent },
  { path: 'reacciones', component: ReaccionComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'clasificaciones', component: ClasificacionComponent },
  { path: 'atributos', component: AtributoComponent },
  { path: 'tipos-de-error', component: TipoDeErrorComponent },
  { path: 'criterios-evaluacion', component: CriteriosEvaluacionComponent },
  { path: 'criterios-reaccion', component: CriteriosReaccionComponent },
  { path: 'asignar-evaluaciones', component: AsignarEvaluacionesComponent },
  { path: 'parametros', component: ParametrosComponent },
  { path: 'resultados-reacciones', component:  ResultadosReaccionesComponent },
  { path: 'evaluaciones', component: EvaluacionesComponent },
  { path: 'avarliciones', component: ArliComponent },
  { path: 'matriz-ponderaciones', component: MatrizPonderacionesComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'negocios' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
