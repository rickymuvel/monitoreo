export class PortafoliosInterface {
	id?:string;
	id_proveedor?:string;
	proveedor?:string;
	id_cartera?:string;
	cartera?:string;
	id_negocio?:string;
	id_jefe_cuenta?:string;
	jefe_cuenta?:string;
	diacorte?:string;
	base?:string;
	estado?:string;
}
