import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { ResultadosReaccionesInterface } from '../../interfaces/resultados-reacciones.interface';
import { ReaccionInterface } from '../../interfaces/reaccion.interface';

import { ResultadosReaccionesService } from '../../servicios/resultados-reacciones.service';
import { ReaccionesService } from '../../servicios/reacciones.service';



@Component({
  selector: 'app-resultados-reacciones',
  templateUrl: './resultados-reacciones.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ResultadosReaccionesComponent {

	msjs:Message[] = [];
	accion:string = "agregar";
	total_registros:string = "";

	ResultadosReacciones:ResultadosReaccionesInterface[] = [];
	Reacciones:ReaccionInterface[] = [];

	display_modal:boolean = false;
	Formulario:FormGroup;
	ResultadosReaccionesSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){
		}
	} 

	constructor(
		private _rrs:ResultadosReaccionesService,
		private _rs:ReaccionesService
		) {
		this.Formulario = new FormGroup({
		  'id': new FormControl(this.objeto.id),
		  'actividad': new FormControl(this.objeto.actividad),
		  'resultado': new FormControl(this.objeto.resultado),
		  'justificacion': new FormControl(this.objeto.justificacion),
		  'negocio': new FormControl(this.objeto.negocio, Validators.required),
		  'contactabilidad': new FormControl(this.objeto.contactabilidad),
		  'reaccion': new FormControl(this.objeto.reaccion, Validators.required),
		  'id_reaccion': new FormControl(this.objeto.id_reaccion, Validators.required),
		  'proveedor': new FormControl(this.objeto.proveedor),
		  'cartera': new FormControl(this.objeto.cartera),
		  'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto:ResultadosReaccionesInterface = {
		id: null,
		actividad:"",
		resultado:"",
		justificacion:"",
		contactabilidad:"",
		reaccion:"",
		id_reaccion: null,
		negocio:"",
		proveedor:"",
		cartera:"",
		estado:""
	}

	objeto_reset:ResultadosReaccionesInterface = {
		id: null,
		actividad:"",
		resultado:"",
		justificacion:"",
		contactabilidad:"",
		id_reaccion: null,
		reaccion:"",
		negocio:"",
		proveedor:"",
		cartera:"",
		estado:""
	}

	handleResultadosReaccionesRowSelect(obj){
		if(this.ResultadosReaccionesSelected==null){
		  this.can_edit = false;
		}
		else{
		  this.can_edit = true;
		}
	}

  crearModal(){
		this.display_modal = true;
  }

  editarModal( dt ){
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.display_modal = true;
	}

	Clonar(r: ResultadosReaccionesInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._rrs.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._rrs.handleError(error);
			let err = error.json();
			this.handleErrorManager(err)
		});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
		this.showMultiple([
			{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
			{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
		]);
	}

	clear(){
		this.msjs = [];
	}

  get(){
	this._rrs.get()
					.subscribe(data => {
						this.total_registros = `${data.data.length} registros encontrados`;
						this.ResultadosReacciones = [];
						this.ResultadosReacciones = data.data;
							if(data.data.length>0){
								this.csv = true;
							}
							else{
								this.csv = false;
							}
					},
					error => {
						this._rrs.handleError(error);
						let err = error.json();
						this.handleErrorManager(err)
					});
		this._rs.get()
						.subscribe(data => {
							this.Reacciones = [];
							this.Reacciones = data.data;
						},
						error => {
							this._rrs.handleError(error);
							let err = error.json();
							this.handleErrorManager(err)
						});
  }

  set(){
		this._rrs.set(this.objeto)
			.subscribe(data => {
			  this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
			  this.ResultadosReacciones = [];
			  this.ResultadosReacciones = data.data;
			  this.display_modal = false;
			},
			error => {
				this._rrs.handleError(error); // AÑADIR EL MÉTODO handleError.
				let err = error.json();
				this.handleErrorManager(err)
			});
  }
}


class ClonRegistro implements ResultadosReaccionesInterface {
   constructor(
		public actividad?,
		public resultado?,
		public justificacion?,
		public contactabilidad?,
		public id_reaccion?,
		public reaccion?,
		public negocio?,
		public proveedor?,
		public cartera?,
		public estado?){}
}
