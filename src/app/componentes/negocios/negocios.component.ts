import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { NegociosInterface } from '../../interfaces/negocio.interface';
import { NegociosService } from '../../servicios/negocios.service';

@Component({
	selector: 'app-negocios',
	templateUrl: './negocios.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class NegociosComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	Negocios:NegociosInterface[] = [];
	display_modal:boolean = false;
	Formulario:FormGroup;
	negociosSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(){

	}
	
	constructor(
		private _ns:NegociosService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'negocio': new FormControl(this.objeto.negocio, Validators.required),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto:NegociosInterface = {
		id: "",
		negocio: "",
		estado: ""
	}

	objeto_reset:NegociosInterface = {
		id: "",
		negocio: "",
		estado: ""
	}

	handleNegociosRowSelect(obj){
		if(this.negociosSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
	}

	crearModal(){
		this.display_modal = true;
		setTimeout(()=>{
			$("#negocio").focus();
		},500);
	}

	editarModal( dt ){
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.display_modal = true;
	}

	get(){
		this._ns.get()
		.subscribe(data => {
			this.Negocios = [];
			this.Negocios = data.data;
			if(data.data.length>0){
				this.csv = true;
			}
			else{
				this.csv = false;
			}
		});
	}

	Clonar(r: NegociosInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	set(){ // vendría a ser Crear(){}
		this._ns.set(this.objeto)
		.subscribe(data =>{
			this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
			this.Negocios = [];
			this.Negocios = data.data;
			this.display_modal = false;
		},
		error => {
			this._ns.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._ns.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._ns.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

	clear(){
		this.msjs = [];
	}

}

class ClonRegistro implements NegociosInterface {
   constructor(
	public id?,
	public negocio?,
	public estado?){}
}
