import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { CarterasInterface } from '../interfaces/cartera.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

import 'rxjs/add/operator/map';


@Injectable()
export class CarterasService {
  constructor(private sett:Settings,
    private _ls:LoginService,
    private _es:ErrorService,
	private http:Http){}

  set(form:CarterasInterface){
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/carteras?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  });
  };

  get(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/carteras?token=${token}`)
						  .map(data => {
							 return data.json();
						  });
  };
  getCarterasUnicas(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/carteras/unicas?token=${token}`)
						  .map(data => {
							 return data.json();
						  });
  };

  update(form:CarterasInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/negocios?token=${token}`, body, { headers })
	    .map(data => {
	      return data.json();
	    })
  }

  handleError(err){
    this._es.handleError(err);
  }
}
