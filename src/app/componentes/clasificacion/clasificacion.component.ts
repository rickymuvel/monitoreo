import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

import { ClasificacionInterface } from '../../interfaces/clasificacion.interface';
declare var $: any;
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { ClasificacionesService } from '../../servicios/clasificaciones.service';

@Component({
	selector: 'app-clasificacion',
	templateUrl: './clasificacion.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class ClasificacionComponent  {

	msjs:Message[] = [];
	accion:string = "agregar";

	Clasificaciones:ClasificacionInterface[] = [];
	display_modal:boolean = false;
	Formulario:FormGroup;
	clasificacionesSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(){

	}
	constructor(
		private _cs:ClasificacionesService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'clasificacion': new FormControl(this.objeto.clasificacion, Validators.required),
			'descripcion': new FormControl(this.objeto.descripcion, Validators.required),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto:ClasificacionInterface = {
		id: "",
		clasificacion: "",
		descripcion: "",
		estado: ""
	}

	objeto_reset:ClasificacionInterface = {
		id: "",
		clasificacion: "",
		descripcion: "",
		estado: ""
	}

	
	handleClasificacionesRowSelect(obj){
		if(this.clasificacionesSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
		}

		crearModal(){
			this.display_modal = true;
			setTimeout(()=>{
				$("#clasificacion").focus();
			},500);
		}

		get(){
		this._cs.get()
			.subscribe(data => {
				this.Clasificaciones = [];
				this.Clasificaciones = data.data;
				if(data.data.length>0){
				this.csv = true;
				}
				else{
				this.csv = false;
				}
			},
			error => {
				this._cs.handleError(error);
				let err = error.json();
				this.handleErrorManager(err);
			});
		}

		set(){
		this._cs.set(this.objeto)
			.subscribe(data =>{
				this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
				this.Clasificaciones = [];
				this.Clasificaciones = data.data;
				this.display_modal = false;
			},
			error => {
				this._cs.handleError(error);
				let err = error.json();
				this.handleErrorManager(err);
			});
		}

		update(){ // vendría a ser Editar(){}
			this.display_modal = false;
			this._cs.update( this.objeto )
			.subscribe(data => {
				this.showSuccess( "Exito", "Hemos actualizado el registro" );
				this.get();
			},
			error => {
				this._cs.handleError(error);
				let err = error.json();
				this.handleErrorManager(err);;
			});
		}

		editarModal( dt ){
			this.accion = "editar";
			let obj = this.Clonar(dt.selection);
			this.Formulario.setValue(obj);
			this.display_modal = true;
		}

		Clonar(r: ClasificacionInterface){
			let objeto = new ClonRegistro();
			for(let prop in r){
				if(this.objeto.hasOwnProperty(prop)){
					objeto[prop] = r[prop];
				}
			}
			return objeto;
		}

		cambiarEstado(){
			this.accion = "agregar";
			this.Formulario.setValue(this.objeto_reset);
		}

		showSuccess( header, mensaje ) {
			this.msjs.push({severity:'success', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showInfo( header, mensaje ) {
			this.msjs.push({severity:'info', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showWarn( header, mensaje ) {
			this.msjs.push({severity:'warn', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showError( header, mensaje ) {
			this.msjs.push({severity:'error', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showMultiple(arreglo:any[]) {
			this.msjs = [];
			for (var i = 0; i < arreglo.length; i++) {
				this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
			}
			setTimeout(()=> this.clear(), 3000);
		}

		handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

		clear(){
			this.msjs = [];
		}

}

class ClonRegistro implements ClasificacionInterface {
	 constructor(
	public id?,
	public clasificacion?,
	public descripcion?,
	public estado?){}
}