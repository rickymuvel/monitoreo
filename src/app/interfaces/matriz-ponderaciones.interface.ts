export class MatrizPonderacionesInterface {
	id?:string;
	reaccion?:string;
	descripcion?:string;
	negocio?:string;
	campana?:string;
	proveedor?:string;
	id_reaccion?:string;
	id_negocio?:string;
	id_cartera?:string;
	id_proveedor?:string;
	id_campana?:string;
	cartera?:string;
	estado?:string;
}
