import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { PortafoliosInterface } from '../../interfaces/portafolio.interface';
import { NegociosInterface } from '../../interfaces/negocio.interface';
import { ProveedorInterface } from '../../interfaces/proveedor.interface';
import { CarterasInterface } from '../../interfaces/cartera.interface';
import { UsuariosInterface } from '../../interfaces/usuarios.interface';

import { PortafolioService } from '../../servicios/portafolio.service';
import { NegociosService } from '../../servicios/negocios.service';
import { ProveedorService } from '../../servicios/proveedor.service';
import { CarterasService } from '../../servicios/carteras.service';
import { UsuarioService } from '../../servicios/usuario.service';

@Component({
	selector: 'app-portafolio',
	templateUrl: './portafolio.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class PortafolioComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	Portafolios:PortafoliosInterface[] = [];
	Negocios:NegociosInterface[] = [];
	Proveedores:ProveedorInterface[] = [];
	Carteras:CarterasInterface[] = [];
	JefesCuenta:UsuariosInterface[] = [];

	display_modal:boolean = false;
	Formulario:FormGroup;
	portafoliosSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;
	hotkeys(event){
			if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){
		}
		}
	
	constructor(
		private _ps:PortafolioService,
		private _ns:NegociosService,
		private _prs:ProveedorService,
		private _us:UsuarioService,
		private _cs:CarterasService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
			'proveedor': new FormControl(this.objeto.proveedor),
			'id_cartera': new FormControl(this.objeto.id_cartera, Validators.required),
			'cartera': new FormControl(this.objeto.cartera),
			'id_negocio': new FormControl(this.objeto.id_negocio, Validators.required),
			'id_jefe_cuenta': new FormControl(this.objeto.id_jefe_cuenta, Validators.required),
			'jefe_cuenta': new FormControl(this.objeto.jefe_cuenta),
			'diacorte': new FormControl(this.objeto.diacorte, Validators.required),
			'base': new FormControl(this.objeto.base, Validators.required),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto:PortafoliosInterface = {
		id: "",
		id_proveedor: "",
		proveedor: "",
		id_cartera: "",
		cartera: "",
		id_negocio: "",
		id_jefe_cuenta: "",
		jefe_cuenta: "",
		diacorte: "",
		base: "",
		estado: "",
	}

	objeto_reset:PortafoliosInterface = {
		id: "",
		id_proveedor: "",
		proveedor: "",
		id_cartera: "",
		cartera: "",
		id_negocio: "",
		id_jefe_cuenta: "",
		jefe_cuenta: "",
		diacorte: "",
		base: "",
		estado: "",
	}

	// cargarBaseDiaCorte(){
	// 	this._ps.getBaseDiaCorte(this.objeto.id_cartera, this.objeto.id_negocio)
	// 			.subscribe(data =>{
	// 				this.objeto.base = data.data[0].base;
	// 				this.objeto.diacorte = data.data[0].diacorte;
	// 			},
	// 			error => {
	// 				this._ps.handleError(error);
	// 			});
	// }

	cargarCarterasxProveedor(){
		return new Promise((resolve, reject) => {
			this._prs.getCarterasxProveedor(this.objeto.id_proveedor)
				.subscribe(data => {
					this.Carteras = [];
					this.Carteras = data.data;
					resolve();
				},
				error=>{
					this._ps.handleError(error);
					reject(new Error(error));
					let err = error.json();
					this.handleErrorManager(err);
				});
		});
	}

	cargarProveedoresxNegocio(){
		return new Promise((resolve, reject) => {
			this._ns.getProveedoresxNegocio(this.objeto.id_negocio)
				.subscribe(data =>{
					this.Proveedores = [];
					this.Proveedores = data.data;
					resolve(); // estamos retornando porque lo necesitamos
				},
				error=>{
					this._ps.handleError(error);
					reject(new Error(error));
					let err = error.json();
					this.handleErrorManager(err);
				})
		});
	}

	editarModal( dt ){
		this.accion = "editar";
		console.log(dt.selection);
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.cargarProveedoresxNegocio()
				.then(() => {
					return true;
				})
				.catch(error => {
					let err = error.json();
					this.handleErrorManager(err);
				});

		this.cargarCarterasxProveedor()
			.then(()=>{
				this.Formulario.reset(this.objeto_reset);
				this.Formulario.setValue(obj);
				this.display_modal = true;
				return true;
			})
	}

	handlePortafoliosRowSelect(obj){
		if(this.portafoliosSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
	}

	crearModal(){
		this.display_modal = true;
		setTimeout(()=>{
			$("#id_negocio").focus();
		},500);
	}

	Clonar(r: PortafoliosInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	get(){
		this._ps.get()
				.subscribe(data => {
					this.Portafolios = [];
					this.Portafolios = data.data;
					if(data.data.length>0){
						this.csv = true;
					}
					else{
						this.csv = false;
					}
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		this._ns.get()
				.subscribe(data => {
					this.Negocios = [];
					this.Negocios = data.data;
				},
				error => {
					this._ns.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		this._prs.get()
				.subscribe(data => {
					this.Proveedores = [];
					this.Proveedores = data.data;
				},
				error => {
					this._prs.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		this._us.getJefeCuenta()
				.subscribe(data => {
					this.JefesCuenta = [];
					this.JefesCuenta = data.data;
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				})
	}

	set(){
		this._ps.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.Portafolios = [];
					this.Portafolios = data.data;
					this.display_modal = false;
				},
				error => {
					this._ps.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._ps.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._ps.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
		this.showMultiple([
			{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
			{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
		]);
	}

	clear(){
		this.msjs = [];
	}
}

class ClonRegistro implements PortafoliosInterface {
   constructor(
		public id?,
		public id_proveedor?,
		public proveedor?,
		public id_cartera?,
		public cartera?,
		public id_negocio?,
		public jefe_cuenta?,
		public diacorte?,
		public base?,
		public estado?){}
}