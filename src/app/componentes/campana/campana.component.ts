import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { CampanasInterface } from '../../interfaces/campana.interface';
import { CampanasService } from '../../servicios/campanas.service';

@Component({
	selector: 'app-campana',
	templateUrl: './campana.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class CampanaComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	Campanas:CampanasInterface[] = [];
	display_modal:boolean = false;
	Formulario:FormGroup;
	campanasSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(event){
	 if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){
		
	 }
	}

	constructor(
		private _cs:CampanasService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'campana': new FormControl(this.objeto.campana, Validators.required),
			'descripcion': new FormControl(this.objeto.descripcion, Validators.required),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}


	objeto:CampanasInterface = {
		id: "",
		campana: "",
		descripcion: "",
		estado: ""
	}

	objeto_reset:CampanasInterface = {
		id: "",
		campana: "",
		descripcion: "",
		estado: ""
	}

	handleCampanasRowSelect(obj){
		if(this.campanasSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
	}

	crearModal(){
		this.display_modal = true;
		setTimeout(()=>{
			$("#campana").focus();
		},500);
	}

	editarModal( dt ){
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.display_modal = true;
	}

	Clonar(r: CampanasInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	get(){
		this._cs.get()
				.subscribe(data => {
					this.Campanas = [];
					this.Campanas = data.data;
					if(data.data.length>0){
						this.csv = true;
					}
					else{
						this.csv = false;
					}
				},
				error => {
					this._cs.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	set(){
		this._cs.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.Campanas = [];
					this.Campanas = data.data;
					this.display_modal = false;
				},
				error => {
					this._cs.handleError(error); // AÑADIR EL MÉTODO handleError.
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	update(){ // vendría a ser Editar(){}
	this.display_modal = false;
	this._cs.update( this.objeto )
	.subscribe(data => {
		this.showSuccess( "Exito", "Hemos actualizado el registro" );
		this.get();
	},
	error => {
		this._cs.handleError(error);
		let err = error.json();
		this.handleErrorManager(err);
	});
}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue( this.objeto_reset );
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
		this.showMultiple([
			{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
			{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
		]);
	}

	clear(){
		this.msjs = [];
	}

}

class ClonRegistro implements CampanasInterface {
   constructor(
	public id?,
	public campana?,
	public descripcion?,
	public estado?){}
}
