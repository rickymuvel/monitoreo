import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { HttpModule } from '@angular/http';


// Rutas
import { APP_ROUTING } from './app.routes';

import { DataTableModule, DialogModule, GrowlModule, CalendarModule, TooltipModule, 
           SidebarModule, DropdownModule } from 'primeng/primeng';

// servicios
import { Settings } from './config';
import { NegociosService } from './servicios/negocios.service';
import { CampanasService } from './servicios/campanas.service';
import { ReaccionesService } from './servicios/reacciones.service';
import { ClasificacionesService } from './servicios/clasificaciones.service';
import { AtributosService } from './servicios/atributos.service';
import { AtributoClasificacionService } from './servicios/atributo-clasificacion.service';
import { TipoDeErrorService } from './servicios/tipo-de-error.service';
import { TipoDeErrorAtributoService } from './servicios/tipo-de-error-atributo.service';
import { CriterioEvaluacionService } from './servicios/criterio-evaluacion.service';
import { CriterioReaccionService } from './servicios/criterio-reaccion.service';
import { PortafolioService } from './servicios/portafolio.service';
import { ProveedorService } from './servicios/proveedor.service';
import { UsuarioService } from './servicios/usuario.service';
import { CarterasService } from './servicios/carteras.service';
import { LoginService } from './servicios/login.service';
import { ErrorService } from './servicios/error.service';
import { ParametrosService } from './servicios/parametros.service';
import { CarteraService } from './servicios/cartera.service';
import { AsignarEvaluacionesService } from './servicios/asignar-evaluaciones.service';
import { ResultadosReaccionesService } from './servicios/resultados-reacciones.service';
import { MatrizPonderacionesService } from './servicios/matriz-ponderaciones.service';


import { AppComponent } from './app.component';
import { NegociosComponent } from './componentes/negocios/negocios.component';
import { CampanaComponent } from './componentes/campana/campana.component';
import { PortafolioComponent } from './componentes/portafolio/portafolio.component';
import { ReaccionComponent } from './componentes/reaccion/reaccion.component';
import { UsuariosComponent } from './componentes/usuarios/usuarios.component';
import { ClasificacionComponent } from './componentes/clasificacion/clasificacion.component';
import { AtributoComponent } from './componentes/atributo/atributo.component';
import { AtributoClasificacionComponent } from './componentes/atributo-clasificacion/atributo-clasificacion.component';
import { TipoDeErrorComponent } from './componentes/tipo-de-error/tipo-de-error.component';
import { TipoErrorAtributoComponent } from './componentes/tipo-error-atributo/tipo-error-atributo.component';
import { CriteriosEvaluacionComponent } from './componentes/criterios-evaluacion/criterios-evaluacion.component';
import { CriteriosReaccionComponent } from './componentes/criterios-reaccion/criterios-reaccion.component';
import { AsignarEvaluacionesComponent } from './componentes/asignar-evaluaciones/asignar-evaluaciones.component';
import { ParametrosComponent } from './componentes/parametros/parametros.component';
import { ArliComponent } from './componentes/negocios/arli.component';
import { EvaluacionesComponent } from './componentes/evaluaciones/evaluaciones.component';
import { MatrizPonderacionesComponent } from './componentes/matriz-ponderaciones/matriz-ponderaciones.component';
import { ResultadosReaccionesComponent } from './componentes/resultados-reacciones/resultados-reacciones.component';


@NgModule({
  declarations: [
    AppComponent,
    NegociosComponent,
    CampanaComponent,
    PortafolioComponent,
    ReaccionComponent,
    UsuariosComponent,
    ClasificacionComponent,
    AtributoComponent,
    AtributoClasificacionComponent,
    TipoDeErrorComponent,
    TipoErrorAtributoComponent,
    CriteriosEvaluacionComponent,
    CriteriosReaccionComponent,
    AsignarEvaluacionesComponent,
    ParametrosComponent,
    EvaluacionesComponent,
    MatrizPonderacionesComponent,
    ArliComponent,
    ResultadosReaccionesComponent
  ],
  imports: [
    APP_ROUTING, HttpModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule,
    DataTableModule, DialogModule, GrowlModule, CalendarModule, TooltipModule, SidebarModule,
    DropdownModule,  
    BrowserModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "es" },
    DatePipe,
    Settings,
    NegociosService,
    CampanasService,
    ReaccionesService,
    ClasificacionesService, 
    AtributosService,
    AtributoClasificacionService,
    TipoDeErrorService,
    TipoDeErrorAtributoService,
    CriterioEvaluacionService,
    CriterioReaccionService,
    PortafolioService, 
    ProveedorService,
    UsuarioService,
    CarterasService,
    ParametrosService,
    CarteraService,
    AsignarEvaluacionesService,
    ResultadosReaccionesService,
    MatrizPonderacionesService,
    LoginService,
    ErrorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
