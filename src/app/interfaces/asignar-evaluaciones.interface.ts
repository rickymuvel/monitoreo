export class AsignarEvaluacionesInterface {
	id?:string;
	id_asig_historica?:string;
	id_usuario?:string;
	evaluador?:string;
	negocio?:string;
	id_negocio?:string;
	id_proveedor?:string;
	proveedor?:string;
	id_cartera?:string;
	cartera?:string;
	nro_agentes_asignados?:string;
	nro_agentes_pendientes?:string;
	estado?:string;
}
