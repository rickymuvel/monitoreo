export class UsuariosInterface {
	id?:string;
	usuario?:string;
	id_proveedor?:string;
	proveedor?:string;
	id_cartera?:string;
	cartera?:string;
	fecha_ingreso_kobsa?:string;
	fecha_ingreso_portafolio?:string;
	condicion_kobsa?:string;
	condicion_portafolio?:string;
	id_jefe_cuenta?:string;
	jefe_cuenta?:string;
	turno?:string;
	estado?:string;
}
