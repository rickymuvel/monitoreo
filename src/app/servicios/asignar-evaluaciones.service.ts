import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';

import { AsignarEvaluacionesInterface } from '../interfaces/asignar-evaluaciones.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

import 'rxjs/add/operator/map';


@Injectable()
export class AsignarEvaluacionesService {
  constructor(
      private sett:Settings,
      private _ls:LoginService,
      private _es:ErrorService,
				  private http:Http) { }

  set( form:AsignarEvaluacionesInterface ){
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/asignar-evaluaciones?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  })
  }

  get(){
   let token = this._ls.getToken();
   return this.http.get(`${this.sett.url}/asignar-evaluaciones?token=${token}`)
              .map(data => {
               return data.json();
              })
  }

  setNuevaAsignacionHistorica(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/asignar-evaluaciones/nueva-asignacion-historica?token=${token}`)
						  .map(data => {
							 return data.json();
						  });
  }

  getEstadoAsignacion(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/asignar-evaluaciones/estado?token=${token}`)
            .map(data => {
             return data.json();
            });
  }

  detenerAsignacion(){
    let token = this._ls.getToken();
    return this.http.get(`${this.sett.url}/asignaciones-historicas/detener?token=${token}`)
            .map(data => {
             return data.json();
            });
  }

  update(form:AsignarEvaluacionesInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/asignar-evaluaciones?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }

}