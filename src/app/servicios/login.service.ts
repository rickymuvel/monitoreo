import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {

  constructor() { }

  getToken(){
  	return localStorage.getItem("token");
  }

}
