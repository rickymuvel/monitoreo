import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { CriterioEvaluacionInterface } from '../../interfaces/criterio-evaluacion.interface';
import { CriterioEvaluacionService } from '../../servicios/criterio-evaluacion.service';

@Component({
  selector: 'app-criterios-evaluacion',
  templateUrl: './criterios-evaluacion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}  
})
export class CriteriosEvaluacionComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	CriterioEvaluaciones:CriterioEvaluacionInterface[] = [];
	display_modal:boolean = false;
	Formulario:FormGroup;
	criterioEvaluacionSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){
		}
	}
	

	constructor(
		private _ces:CriterioEvaluacionService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'criterio': new FormControl(this.objeto.criterio, Validators.required),
			'prioridad': new FormControl(this.objeto.prioridad, Validators.required),
			'peso': new FormControl(this.objeto.peso, Validators.required),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto:CriterioEvaluacionInterface = {
	    id: '',
		criterio: '',
		prioridad: '',
		peso: '',
		estado: '',
  	}

  	objeto_reset:CriterioEvaluacionInterface = {
	    id: '',
		criterio: '',
		prioridad: '',
		peso: '',
		estado: '',
  	}

  	handleCriterioEvaluacionRowSelect(obj){
			if(this.criterioEvaluacionSelected==null){
				this.can_edit = false;
			}
			else{
				this.can_edit = true;
			}
		}

	crearModal(){
		this.display_modal = true;
		setTimeout(()=>{
			$("#campana").focus();
		},500);
	}

	editarModal( dt ){
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.display_modal = true;
	}

	Clonar(r: CriterioEvaluacionInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	get(){
		this._ces.get()
				.subscribe(data => {
					this.CriterioEvaluaciones = [];
					this.CriterioEvaluaciones = data.data;
					if(data.data.length>0){
						this.csv = true;
					}
					else{
						this.csv = false;
					}
				},
				error => {
					this._ces.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	set(){
		this._ces.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.CriterioEvaluaciones = [];
					this.CriterioEvaluaciones = data.data;
					this.display_modal = false;
				},
			error => {
				this._ces.handleError(error);
				let err = error.json();
				this.handleErrorManager(err);
			});
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._ces.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._ces.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
		this.showMultiple([
			{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
			{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
		]);
	}

	clear(){
		this.msjs = [];
	}


}


class ClonRegistro implements CriterioEvaluacionInterface {
   constructor(
		public id?,
		public criterio?,
		public prioridad?,
		public peso?,
		public estado?){}
}