export class CriterioEvaluacionInterface {
	id?:string;
	criterio?:string;
	prioridad?:string;
	peso?:string;
	estado?:string;
}
