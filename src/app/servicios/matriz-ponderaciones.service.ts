import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { MatrizPonderacionesInterface } from '../interfaces/matriz-ponderaciones.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

import 'rxjs/add/operator/map';

@Injectable()
export class MatrizPonderacionesService {
  constructor(
    private sett:Settings,
    private _ls:LoginService,
    private _es:ErrorService,
    private http:Http
    ) {}

  set(form:MatrizPonderacionesInterface){
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/matriz-ponderaciones?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  })
  }

  get(id_negocio, id_campana, id_proveedor, id_cartera){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/matriz-ponderaciones/${id_negocio}/${id_campana}/${id_proveedor}/${id_cartera}?token=${token}`)
						  .map(data => {
							 return data.json();
						  });
  }

  update(form:MatrizPonderacionesInterface) {
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/matriz-ponderaciones?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }
}