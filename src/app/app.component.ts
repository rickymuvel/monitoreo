import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'},
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';
	display_hp:boolean = false;
	Coincidencia:number[] = [77,73,67,72,73,84,65,66,79,78,73,84,65];
	cont_concidencia = 0;
	hotkeys(event){
		if(this.Coincidencia[this.cont_concidencia]==event.keyCode){
			this.cont_concidencia++;
		}
		else{
			this.cont_concidencia = 0;
		}
		if(this.cont_concidencia==13){
			this.display_hp = true;
			this.cont_concidencia = 0;
		}
	}
}
