import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';

import { CarterasInterface } from '../interfaces/cartera.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

// import { TipoProductoInterface } from '../interfaces/tipo-producto.interface';
// import { ProductoInterface } from '../interfaces/producto.interface';
import 'rxjs/add/operator/map';


@Injectable()
export class CarteraService {
	constructor(
		private sett:Settings,
		private _ls:LoginService,
		private _es:ErrorService,
		private http:Http) { }

	get(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/carteras?token=${token}`)
							.map(data => {
							 return data.json();
							})
	}

	getCarterasUnicas(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/carteras/unicas?token=${token}`)
							.map(data => {
							 return data.json();
							})
	}

	getNumAgentes(id_cartera){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/carteras/num_agentes/${id_cartera}?token=${token}`)
							.map(data => {
							 return data.json();
							})
	}

	handleError(err){
		this._es.handleError(err);
	}

}
