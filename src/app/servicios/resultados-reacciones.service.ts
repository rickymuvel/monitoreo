import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';

import { ResultadosReaccionesInterface } from '../interfaces/resultados-reacciones.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

// import { TipoProductoInterface } from '../interfaces/tipo-producto.interface';
// import { ProductoInterface } from '../interfaces/producto.interface';
import 'rxjs/add/operator/map';


@Injectable()
export class ResultadosReaccionesService {
	constructor(
		private sett:Settings,
		private _ls:LoginService,
		private _es:ErrorService,
		private http:Http) { }

	set( form:ResultadosReaccionesInterface ){
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/resultados-reacciones?token=${token}`, body, { headers })
							.map(data => {
							 return data.json();
							})
	}

	get(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/resultados-reacciones?token=${token}`)
							.map(data => {
								return data.json();
							})
	}

	update( form:ResultadosReaccionesInterface ){
		let token = this._ls.getToken();
		let body = JSON.stringify(form);
		let headers = new Headers({
			'Content-Type': 'application/json'
		});
		return this.http.put(`${this.sett.url}/resultados-reacciones?token=${token}`, body, { headers })
										.map(data => {
											return data.json();
										});
	}

	handleError(err){
		this._es.handleError(err);
	}

}
