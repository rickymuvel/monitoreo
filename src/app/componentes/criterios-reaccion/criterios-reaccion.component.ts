import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { CriterioReaccionInterface } from '../../interfaces/criterio-reaccion.interface';
import { CriterioEvaluacionInterface } from '../../interfaces/criterio-evaluacion.interface';
import { ReaccionInterface } from '../../interfaces/reaccion.interface';

import { CriterioReaccionService } from '../../servicios/criterio-reaccion.service';
import { CriterioEvaluacionService } from '../../servicios/criterio-evaluacion.service';
import { ReaccionesService } from '../../servicios/reacciones.service';

@Component({
  selector: 'app-criterios-reaccion',
  templateUrl: './criterios-reaccion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}  
})
export class CriteriosReaccionComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	CriterioReacciones:CriterioReaccionInterface[] = [];
	CriterioEvaluaciones:CriterioEvaluacionInterface[] = [];
	Reacciones:ReaccionInterface[] = [];
	display_modal:boolean = false;
	Formulario:FormGroup;
	criterioReaccionSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){
		}
	}
	

	constructor(
		private _crs:CriterioReaccionService,
		private _ces:CriterioEvaluacionService,
		private _rs:ReaccionesService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'id_reaccion': new FormControl(this.objeto.id_reaccion, Validators.required),
			'reaccion': new FormControl(this.objeto.reaccion, Validators.required),
			'id_criterio': new FormControl(this.objeto.id_criterio),
			'criterio': new FormControl(this.objeto.criterio),
			'prioridad': new FormControl(this.objeto.prioridad),
			'peso': new FormControl(this.objeto.peso),
			'estado': new FormControl(this.objeto.estado, Validators.required)
		});
		this.get();
	}

	objeto:CriterioReaccionInterface = {
	    id: '',
		id_reaccion: '',
		reaccion: '',
		id_criterio: '',
		criterio: '',
		prioridad: '',
		peso: '',
		estado: ''
  	}

  	objeto_reset:CriterioReaccionInterface = {
	    id: '',
		id_reaccion: '',
		reaccion: '',
		id_criterio: '',
		criterio: '',
		prioridad: '',
		peso: '',
		estado: ''
  	}

  	buscarPrioridadPeso(id_criterio){
  		let obj = {id_criterio: id_criterio};
  		this._ces.getPrioridadPeso(obj)
  				 .subscribe(data =>{
  				 	this.objeto.prioridad = data.data[0].prioridad;
  				 	this.objeto.peso = data.data[0].peso;
  				 });
  	}

  	handleCriterioReaccionRowSelect(obj){
		if(this.criterioReaccionSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
	}

	crearModal(){
		this.display_modal = true;
		setTimeout(()=>{
			$("#reaccion").focus();
		},500);
	}

	get(){
		this._crs.get()
				.subscribe(data => {
					this.CriterioReacciones = [];
					this.CriterioReacciones = data.data;
					if(data.data.length>0){
						this.csv = true;
					}
					else{
						this.csv = false;
					}
				});
		this._ces.get()
				.subscribe(data => {
					this.CriterioEvaluaciones = [];
					this.CriterioEvaluaciones = data.data;
				});
		this._rs.get()
				.subscribe(data => {
					this.Reacciones = [];
					this.Reacciones = data.data;
				});
	}

	set(){
		this._crs.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.CriterioReacciones = [];
					this.CriterioReacciones = data.data;
					this.display_modal = false;
				},
				error => {
					this._crs.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}


	editarModal( dt ){
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.display_modal = true;
	}

	Clonar(r: CriterioReaccionInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._crs.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._crs.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
		this.showMultiple([
			{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
			{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
		]);
	}

	clear(){
		this.msjs = [];
	}


}

class ClonRegistro implements CriterioReaccionInterface {
   constructor(
	public id?,
	public id_reaccion?,
	public reaccion?,
	public id_criterio?,
	public criterio?,
	public prioridad?,
	public peso?,
	public estado?){}
}