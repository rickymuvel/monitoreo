import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { AsignarEvaluacionesInterface } from '../../interfaces/asignar-evaluaciones.interface';
import { UsuariosInterface } from '../../interfaces/usuarios.interface';
import { NegociosInterface } from '../../interfaces/negocio.interface';
import { ProveedorInterface } from '../../interfaces/proveedor.interface';
import { CarterasInterface } from '../../interfaces/cartera.interface';

import { AsignarEvaluacionesService } from '../../servicios/asignar-evaluaciones.service';
import { UsuarioService } from '../../servicios/usuario.service';
import { NegociosService } from '../../servicios/negocios.service';
import { ProveedorService } from '../../servicios/proveedor.service';
import { CarteraService } from '../../servicios/cartera.service';

@Component({
	selector: 'app-asignar-evaluaciones',
	templateUrl: './asignar-evaluaciones.component.html',
	host: {'(window:keydown)': 'hotkeys($event)'}
})
export class AsignarEvaluacionesComponent  {

	msjs:Message[] = [];
	accion:string = "asignar";
	id_asignacion_historica:any;

	AsignarEvaluaciones:AsignarEvaluacionesInterface[] = [];
	Usuarios:UsuariosInterface[] = [];
	Negocios:NegociosInterface[] = [];
	Proveedores:ProveedorInterface[] = [];
	Carteras:CarterasInterface[] = [];
	

	display_modal:boolean = false;
	display_modal_confirmation_nueva:boolean = false;
	display_modal_confirmation_stop:boolean = false;
	Formulario:FormGroup;
	asignarEvaluacionesSelected:Object;

	estado_asignacion:boolean = false; // inactivo

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(){}

	constructor(
		private _aes:AsignarEvaluacionesService,
		private _us:UsuarioService,
		private _ns:NegociosService,
		private _ps:ProveedorService,
		private _cs:CarteraService
		) {
		this.Formulario = new FormGroup({
			'id': new FormControl(this.objeto.id),
			'id_asig_historica': new FormControl(this.objeto.id_asig_historica),
			'evaluador': new FormControl(this.objeto.evaluador),
			'negocio': new FormControl(this.objeto.negocio),
			'proveedor': new FormControl(this.objeto.proveedor),
			'cartera': new FormControl(this.objeto.cartera),
			'id_usuario': new FormControl(this.objeto.id_usuario, Validators.required),
			'id_negocio': new FormControl(this.objeto.id_negocio, Validators.required),
			'id_cartera': new FormControl(this.objeto.id_cartera, Validators.required),
			'id_proveedor': new FormControl(this.objeto.id_proveedor, Validators.required),
			'nro_agentes_asignados': new FormControl(this.objeto.nro_agentes_asignados, Validators.required),
			'nro_agentes_pendientes': new FormControl(this.objeto.nro_agentes_pendientes),
			'estado': new FormControl(this.objeto.estado)
		});
		this.get();
		this.getEstadoAsignacion();
	}

	objeto:AsignarEvaluacionesInterface = {
		id: "",
		id_asig_historica: "",
		id_usuario: "",
		evaluador: "",
		negocio: "",
		id_negocio: "",
		id_proveedor: "",
		proveedor: "",
		id_cartera: "",
		cartera: "",
		nro_agentes_asignados: "",
		nro_agentes_pendientes: "",
		estado: ""
	};

	objeto_reset:AsignarEvaluacionesInterface = {
		id: "",
		id_asig_historica: "",
		id_usuario: "",
		evaluador: "",
		negocio: "",
		id_negocio: "",
		id_proveedor: "",
		proveedor: "",
		id_cartera: "",
		cartera: "",
		nro_agentes_asignados: "",
		nro_agentes_pendientes: "",
		estado: ""
	};

	confirmarNuevaAsignacion(){
		this.display_modal_confirmation_nueva=false;
		this.display_modal=true
		this._aes.setNuevaAsignacionHistorica()
						 .subscribe(data => {
								this.estado_asignacion = true;
								this.id_asignacion_historica = data.id_asignacion_historica;
								this.objeto.id_asig_historica = data.id_asignacion_historica;
								this.anadirEvaluador();
						 },
						 error => {
						 	this._aes.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						 })
	}

	getEstadoAsignacion(){
		this._aes.getEstadoAsignacion()
						 .subscribe(data =>{
						 	this.objeto.id_asig_historica = data.id_historico;
						 	this.id_asignacion_historica = data.id_historico;
						 	this.estado_asignacion = data.estado;
						 },
						 error => {
						 	this._aes.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						 });
	}

	detenerAsignacion(){
		this.display_modal_confirmation_stop = false;
		this._aes.detenerAsignacion()
						 .subscribe(data=>{
						 	if(data.estado==true){
						 		this.get();
						 		this.getEstadoAsignacion();
						 	}
						 },
						 error => {
						 	this._aes.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						 })
	}
	
	anadirEvaluador(){
		// extraer los evaluadores que estén disponibles
	 	this._us.getEvaluadores(this.id_asignacion_historica)
						.subscribe(data => {
							this.Usuarios = data.data;
							this.display_modal = true;
						},
						error => {
							this._us.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
	}



	abrirModalConfirmacion(){
		this.display_modal_confirmation_nueva = true;
	}

	abrirModalConfirmacionDetener(){
		this.display_modal_confirmation_stop = true;
	}

	handleAsignarEvaluacionesRowSelect(obj){
		if(this.asignarEvaluacionesSelected==null){
			this.can_edit = false;
		}
		else{
			this.can_edit = true;
		}
	}

	extraerNumAgentes(id_cartera){
		this._cs.getNumAgentes(id_cartera)
						.subscribe(data => {
							this.objeto.nro_agentes_asignados = data.data[0].cant;
							this.objeto.nro_agentes_pendientes = data.data[0].cant;
						},
						error => {
							this._cs.handleError(error);
							let err = error.json();
							this.handleErrorManager(err);
						});
	}

		crearModal(){
			this.display_modal = true;
			setTimeout(()=>{
				$("#clasificacion").focus();
			},500);
		}

		get(){
			this._aes.get()
				.subscribe(data => {
					this.AsignarEvaluaciones = [];
					this.AsignarEvaluaciones = data.data;
					if(data.data.length>0){
					this.csv = true;
					}
					else{
					this.csv = false;
					}
				},
				error => {
					this._aes.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});

		this._ns.get()
						.subscribe(data => {
							this.Negocios = data.data;
						},
						error => {
						this._ns.handleError(error);
						let err = error.json();
						this.handleErrorManager(err);
					});
		}

		ProveedorxNegocio(id_negocio){
			this._ns.getProveedoresxNegocio(id_negocio)
							.subscribe(data => {
								this.Proveedores = data.data;
							},
							error => {
								this._ns.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
							});
		}

		CarterasxProveedor(id_proveedor){
			this._ps.getCarterasxProveedor(id_proveedor)
							.subscribe(data => {
								this.Carteras = data.data;
							},
							error => {
								this._ps.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
							});
		}

		set(){
			this.objeto.id_asig_historica = this.id_asignacion_historica;
			this._aes.set(this.objeto)
				.subscribe(data =>{
					this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
					this.AsignarEvaluaciones = [];
					this.AsignarEvaluaciones = data.data;
					this.display_modal = false;
				},
				error => {
					this._aes.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
		}

		update(){ // vendría a ser Editar(){}
			this.display_modal = false;
			this._aes.update( this.objeto )
			.subscribe(data => {
				this.showSuccess( "Exito", "Hemos actualizado el registro" );
				this.get();
			},
			error => {
				this._aes.handleError(error);
				let err = error.json();
				this.handleErrorManager(err);
			});
		}

		editarModal( dt ){
			this.accion = "editar";
			dt.selection.id_asig_historica = this.id_asignacion_historica;
			let obj = this.Clonar(dt.selection);
			this.Formulario.setValue(obj);
			this.display_modal = true;
		}

		Clonar(r: AsignarEvaluacionesInterface){
			let objeto = new ClonRegistro();
			for(let prop in r){
				if(this.objeto.hasOwnProperty(prop)){
					objeto[prop] = r[prop];
				}
			}
			return objeto;
		}

		cambiarEstado(){
			this.accion = "asignar";
			this.Formulario.setValue(this.objeto_reset);
		}

		showSuccess( header, mensaje ) {
			this.msjs.push({severity:'success', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showInfo( header, mensaje ) {
			this.msjs.push({severity:'info', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showWarn( header, mensaje ) {
			this.msjs.push({severity:'warn', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showError( header, mensaje ) {
			this.msjs.push({severity:'error', summary: header, detail: mensaje});
			setTimeout(()=> this.clear(), 3000);
		}

		showMultiple(arreglo:any[]) {
			this.msjs = [];
			for (var i = 0; i < arreglo.length; i++) {
				this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
			}
			setTimeout(()=> this.clear(), 5000);
		}

		clear(){
			this.msjs = [];
		}

		handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

}

class ClonRegistro implements AsignarEvaluacionesInterface {
constructor(
	public id?,
	public id_asig_historica?,
	public id_usuario?,
	public evaluador?,
	public negocio?,
	public id_negocio?,
	public id_proveedor?,
	public proveedor?,
	public id_cartera?,
	public cartera?,
	public nro_agentes_asignados?,
	public nro_agentes_pendientes?,
	public estado?){}
}