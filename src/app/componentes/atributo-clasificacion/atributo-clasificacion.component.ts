import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { AtributoClasificacionInterface } from '../../interfaces/atributo-clasificacion.interface';

import { AtributoClasificacionService } from '../../servicios/atributo-clasificacion.service';
import { ClasificacionesService } from '../../servicios/clasificaciones.service';
import { AtributosService } from '../../servicios/atributos.service';

@Component({
  selector: 'app-atributo-clasificacion',
  templateUrl: './atributo-clasificacion.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class AtributoClasificacionComponent {

	msjs:Message[] = [];
	accion:string = "agregar";

	AtributosClasificaciones:AtributoClasificacionInterface[] = [];
	Atributos:any[] = [];
	Clasificaciones:any[] = [];
	display_modal:boolean = false;
	Formulario:FormGroup;
  	atributoClasificacionSelected:Object;

	can_edit:boolean = false;
	csv:boolean = false;

	hotkeys(event){
		if(event.altKey && event.keyCode==71 && event.ctrlKey && event.shiftKey){}
	}
	constructor(
		private _acs:AtributoClasificacionService,
		private _as:AtributosService,
		private _cs:ClasificacionesService,
		) {
		this.Formulario = new FormGroup({
	      'id': new FormControl(this.objeto.id),
	      'id_clasificacion': new FormControl(this.objeto.id_clasificacion, Validators.required),
	      'clasificacion': new FormControl(this.objeto.clasificacion, Validators.required),
	      'id_atributo': new FormControl(this.objeto.id_atributo, Validators.required),
	      'atributo': new FormControl(this.objeto.atributo, Validators.required),
	      'estado': new FormControl(this.objeto.estado, Validators.required)
	    });
	    this.get();		
	}

	objeto:AtributoClasificacionInterface = {
		id: "",
		id_clasificacion: "",
		clasificacion: "",
		id_atributo: "",
		atributo: "",
		estado: ""
  	}

  	objeto_reset:AtributoClasificacionInterface = {
		id: "",
		id_clasificacion: "",
		clasificacion: "",
		id_atributo: "",
		atributo: "",
		estado: ""
  	}

  	handleAtributoClasificacionRowSelect(obj){
	    if(this.atributoClasificacionSelected==null){
	      this.can_edit = false;
	    }
	    else{
	      this.can_edit = true;
	    }
	 }

	crearModal(){
	    this.display_modal = true;
	}

	get(){
		this._acs.get()
			.subscribe(data => {
				this.AtributosClasificaciones = [];
				this.AtributosClasificaciones = data.data;
				if(data.data.length>0){
					this.csv = true;
				}
				else{
					this.csv = false;
				}
			},
			error => {
				this._acs.handleError(error);
				let err = error.json();
				this.handleErrorManager(err);
			});

		// Obteniendo atributos
		this._as.get()
				.subscribe(data => {
					this.Atributos = [];
					this.Atributos = data.data;
				},
				error => {
					this._as.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});

		// Obteniendo clasificaciones
		this._cs.get()
				.subscribe(data => {
					this.Clasificaciones = [];
					this.Clasificaciones = data.data;
				},
				error => {
					this._cs.handleError(error);
					let err = error.json();
					this.handleErrorManager(err);
				});
	}

	set(){
	this._acs.set(this.objeto)
	    .subscribe(data =>{
	      this.Formulario.reset(this.objeto_reset); // reseteamos el formulario
	      this.AtributosClasificaciones = [];
	      this.AtributosClasificaciones = data.data;
	      this.display_modal = false;
	    },
		error => {
			this._acs.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	editarModal( dt ){
		this.accion = "editar";
		let obj = this.Clonar(dt.selection);
		this.Formulario.setValue(obj);
		this.display_modal = true;
	}

	Clonar(r: AtributoClasificacionInterface){
		let objeto = new ClonRegistro();
		for(let prop in r){
			if(this.objeto.hasOwnProperty(prop)){
				objeto[prop] = r[prop];
			}
		}
		return objeto;
	}

	update(){ // vendría a ser Editar(){}
		this.display_modal = false;
		this._acs.update( this.objeto )
		.subscribe(data => {
			this.showSuccess( "Exito", "Hemos actualizado el registro" );
			this.get();
		},
		error => {
			this._acs.handleError(error);
			let err = error.json();
			this.handleErrorManager(err);
		});
	}

	cambiarEstado(){
		this.accion = "agregar";
		this.Formulario.setValue(this.objeto_reset);
	}

	showSuccess( header, mensaje ) {
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

	clear(){
		this.msjs = [];
	}
}

class ClonRegistro implements AtributoClasificacionInterface {
   constructor(
	public id?,
	public id_clasificacion?,
	public clasificacion?,
	public id_atributo?,
	public atributo?,
	public estado?){}
}