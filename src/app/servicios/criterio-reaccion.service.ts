import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Settings } from '../config';
import { CriterioReaccionInterface } from '../interfaces/criterio-reaccion.interface';
import { LoginService } from './login.service'
import { ErrorService } from './error.service'

import 'rxjs/add/operator/map';


@Injectable()
export class CriterioReaccionService {
  constructor(
        private sett:Settings,
        private _ls:LoginService,
        private _es:ErrorService,
        private http:Http) { }

  set(form:CriterioReaccionInterface){
	 let token = this._ls.getToken();
	 let body = JSON.stringify(form);
	 let headers = new Headers({
		'Content-Type': 'application/json'
	 });

	 return this.http.post(`${this.sett.url}/reaccion-criterio?token=${token}`, body, { headers })
						  .map(data => {
							 return data.json();
						  })
  }

  get(){
	 let token = this._ls.getToken();
	 return this.http.get(`${this.sett.url}/reaccion-criterio?token=${token}`)
						  .map(data => {
							 return data.json();
						  })
  }

  update(form:CriterioReaccionInterface){
    let token = this._ls.getToken();
    let body = JSON.stringify(form);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put(`${this.sett.url}/reaccion-criterio?token=${token}`, body, { headers })
    .map(data => {
      return data.json();
    })
  }

  handleError(err){
    this._es.handleError(err);
  }

}
