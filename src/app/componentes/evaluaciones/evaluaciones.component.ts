import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';

declare var $: any;

import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";

import { EvaluacionesInterface } from '../../interfaces/evaluaciones.interface';
import { UsuariosInterface } from '../../interfaces/usuarios.interface';

import { UsuarioService } from '../../servicios/usuario.service';


@Component({
  selector: 'app-evaluaciones',
  templateUrl: './evaluaciones.component.html',
  host: {'(window:keydown)': 'hotkeys($event)'}
})
export class EvaluacionesComponent {

	can_edit:boolean = false;
	csv:string = "";
	msjs:Message[] = [];

	Agentes:UsuariosInterface[] = [];

	hotkeys(event){
	    if(event.keyCode==69 && event.ctrlKey && event.shiftKey){
	    	this.showSidebar();
		}
		if(event.keyCode==27){
	    	this.hideSidebar();
		}
  	} 

  	Gestiones:EvaluacionesInterface[] = [];
  	display_sidebar:boolean = false;

	constructor(
		private _us:UsuarioService
		) {

		this.getAgentes();
	}

	objeto:EvaluacionesInterface = {
	    id: '',
			documento: '',
			cliente: '',
			telefono: '',
			fecha: '',
			negocio: '',
			status_audio: '',
			nota: '',
			comentarios: '',
			validar_email: '',
  	}

  	objeto_reset:EvaluacionesInterface = {
	    id: '',
			documento: '',
			cliente: '',
			telefono: '',
			fecha: '',
			negocio: '',
			status_audio: '',
			nota: '',
			comentarios: '',
			validar_email: '',
	  	}

  	showSidebar(){
  		this.display_sidebar = true;
  	}

  	hideSidebar(){
  		this.display_sidebar = false;
  	}



  	getAgentes(){
  		let id_monitor = 1470;
  		this._us.getAgentes(id_monitor)
  						.subscribe(data => {
  							this.Agentes = [];
  							this.Agentes = data.data;
  						},
  						error => {
  							this._us.handleError(error);
								let err = error.json();
								this.handleErrorManager(err);
  						});
  	}

  	handleAgentesRowSelect(event){
  		
  	}

  	cargarGestiones(dt){
  		console.log(dt.selection);
  	}

  	showSuccess( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'success', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showInfo( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'info', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showWarn( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'warn', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showError( header, mensaje ) {
		this.msjs = [];
		this.msjs.push({severity:'error', summary: header, detail: mensaje});
		setTimeout(()=> this.clear(), 3000);
	}

	showMultiple(arreglo:any[]) {
		this.msjs = [];
		for (var i = 0; i < arreglo.length; i++) {
			this.msjs.push({severity:arreglo[i].tipo, summary: arreglo[i].titulo, detail: arreglo[i].descripcion});
		}
		setTimeout(()=> this.clear(), 3000);
	}

	handleErrorManager(err){
			this.showMultiple([
				{titulo: "Error", descripcion: `Código de error ${ err.codigo }`, tipo: "error" },
				{titulo: "Importante", descripcion: "Verifique la información que está manipulando. Informe a sistemas del código de error", tipo: "info" }
			]);
		}

	clear(){
		this.msjs = [];
	}
}
